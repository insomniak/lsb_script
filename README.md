# lsb_script
A script to decrypt or encrypt something in images with the LSB method.

# Dependencies
- PIL

## Use:

Use ./lsb.py and follow the instruction

```
    ./lsb.py -h
```