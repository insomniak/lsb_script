#!/usr/bin/python3

import sys, getopt, PIL
from PIL import Image

# Select the image with an input
def     selectImage():
    image = '0';
    entry = input("Enter the image: ");
    image = Image.open(entry);
    return (image);

# Select the text to hide in the Image
def     entry_text():
    entry_to_hide = input("Enter the text to hide: \n");
    return (entry_to_hide);

# Encrypt text in image
def     encrypt_in_image(space, image, text, easy):
    count = 0;
    index = 0;
    bit = 7;
    width, height = image.size;
    if len(text) > (width * height) / 6:
        print ("error Text to long for the Image");
        return (-1);
    for y in range(height):
        for x in range(width):
            if count % space == 0:
                pix = list(image.getpixel((x, y)))
                for i in range(3):
                    if index == len(text):
                        image.putpixel((x, y), tuple(pix))
                        return
                    if (easy):
                        pix[i] = 132 * bit % 255
                    pix[i] = (pix[i] & ~1) | ((ord(text[index]) >> bit) & 1);
                    bit -= 1;
                    if bit < 0:
                        bit = 7;
                        index += 1;
                image.putpixel((x, y), tuple(pix));
            count += 1;

# The Encode choice who manage all actions
# for encode a text in image with LSB method.
def     encode(space, easy):
    image = selectImage();
    entry_to_hide = entry_text();
    encrypt_in_image(space, image, entry_to_hide, easy);
    image.save(input("Enter the name of the encrypted image: "));
    image.show();

# Extract the text from the image.
def     decrypt_text(space, image, length):
    count = 0;
    text = "";
    letter = '0';
    bit = 7;
    width, height = image.size;
    for y in range(height):
        for x in range(width):
            if count % space == 0:
                pix = image.getpixel((x, y))
                for i in range(3):
                    letter = (ord(letter) & ~(1 << bit)) | ((pix[i] & 1) << bit);
                    letter = chr(letter);
                    bit -= 1;
                    if bit < 0:
                        text += letter;
                        if (len(text) >= length):
                            print(text);
                            return
                        bit = 7;
            count += 1;
    print(text);

# The decode choice who manage all actions
# for decode an image and extract text.
def     decode(space):
    image = selectImage();
    length = eval(input("Enter the length of the hidden text to find: "))
    decrypt_text(space, image, length);

def     parsing(argv):
    space = 1;
    easy = 0;
    try:
        opts, args = getopt.getopt(argv, "hs:e", ["space=", "easy", "help"]);
    except getopt.GetoptError:
        print('lsb.py [OPTION]');
        sys.exit(2);
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print('Usage: lsb.py [OPTION]');
            print('Option:');
            print('     -s <nbrspace>, --space=<nbrspace>   number of space between pixels by default 1');
            print('     -e, --easy                          set the encode mode in easy');
            sys.exit();
        elif opt in ("-s", "--space"):
            space = arg;
        elif opt in ("-e", "--easy"):
            easy = 1;
    return (space, easy);

# Main function
def     main(argv):
    space, easy = parsing(argv);
    entry = "";
    print("The space between pixels will be:", space);
    print("Do you want to encode or decode?");
    print("1- Encode");
    print("2- Decode");
    entry = input("Your choice: ")
    if ((entry) == "1"):
        encode(space, easy)
    elif entry == "2":
        decode(space)
    else:
        print("Error wrong choice")
        sys.exit();

if __name__ == "__main__":
    main(sys.argv[1:]);
